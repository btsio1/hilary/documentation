--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

-- Started on 2023-06-01 20:31:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE pharmagest;
--
-- TOC entry 3515 (class 1262 OID 16565)
-- Name: pharmagest; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE pharmagest WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United Kingdom.1252';


\connect pharmagest

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 3516 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 903 (class 1247 OID 24946)
-- Name: modeprise; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.modeprise AS ENUM (
    'Orale',
    'Nasale',
    'Injectable',
    'ApplicationCutanée'
);


--
-- TOC entry 900 (class 1247 OID 24938)
-- Name: typemed; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.typemed AS ENUM (
    'Antibiothique',
    'Antihistaminique',
    'analgersique'
);


--
-- TOC entry 242 (class 1255 OID 16597)
-- Name: addlogin(integer, character varying, character varying, character varying, character varying, character varying, character varying); Type: PROCEDURE; Schema: public; Owner: -
--

CREATE PROCEDURE public.addlogin(INOUT loid integer, IN lonom character varying, IN loprenom character varying, IN lousername character varying, IN lopassword character varying, IN loaddress character varying, IN lotelephone character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
	INSERT INTO login (nom,prenom,username,password,address,telephone)VALUES
	(loNom,
	 loPrenom,
	 loUsername,
	 loPassword,
	 loAddress,
	 loTelephone
	)RETURNING Id INTO loId; 
END
$$;


--
-- TOC entry 243 (class 1255 OID 90757)
-- Name: ajouter_fournisseur(integer, character varying, character varying); Type: PROCEDURE; Schema: public; Owner: -
--

CREATE PROCEDURE public.ajouter_fournisseur(IN p_idfournisseur integer, IN p_numtel character varying, IN p_nom character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO public.fournisseur (idfournisseur, numtel, nom)
    VALUES (p_idfournisseur, p_numtel, p_nom);
END;
$$;


--
-- TOC entry 255 (class 1255 OID 25284)
-- Name: historique_client(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.historique_client() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO historique_client (id_client, nom, prenom, date_naissance, adresse, telephone, email, date_modification, operation)
        VALUES (NEW.id_client, NEW.nom, NEW.prenom, NEW.date_naissance, NEW.adresse, NEW.telephone, NEW.email, now(), 'INSERT');
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO historique_client (id_client, nom, prenom, date_naissance, adresse, telephone, email, date_modification, operation)
        VALUES (OLD.id_client, OLD.nom, OLD.prenom, OLD.date_naissance, OLD.adresse, OLD.telephone, OLD.email, now(), 'UPDATE');
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO historique_client (id_client, nom, prenom, date_naissance, adresse, telephone, email, date_modification, operation)
        VALUES (OLD.id_client, OLD.nom, OLD.prenom, OLD.date_naissance, OLD.adresse, OLD.telephone, OLD.email, now(), 'DELETE');
    END IF;
    RETURN NEW;
END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 222 (class 1259 OID 24845)
-- Name: achat; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.achat (
    idachat integer NOT NULL,
    prix double precision,
    idclient integer
);


--
-- TOC entry 221 (class 1259 OID 24844)
-- Name: achat_idachat_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.achat_idachat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3517 (class 0 OID 0)
-- Dependencies: 221
-- Name: achat_idachat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.achat_idachat_seq OWNED BY public.achat.idachat;


--
-- TOC entry 223 (class 1259 OID 24856)
-- Name: chefpharmacie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.chefpharmacie (
    idchef integer NOT NULL,
    email character varying(50) DEFAULT NULL::character varying,
    password character varying(50) DEFAULT NULL::character varying
);


--
-- TOC entry 220 (class 1259 OID 24833)
-- Name: client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.client (
    idclient integer NOT NULL,
    numsocial character varying(50) DEFAULT NULL::character varying,
    maladiechronique integer
);


--
-- TOC entry 225 (class 1259 OID 24869)
-- Name: commande; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.commande (
    idcommande integer NOT NULL,
    prix double precision,
    idclient integer NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 24868)
-- Name: commande_idcommande_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.commande_idcommande_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3518 (class 0 OID 0)
-- Dependencies: 224
-- Name: commande_idcommande_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.commande_idcommande_seq OWNED BY public.commande.idcommande;


--
-- TOC entry 227 (class 1259 OID 24881)
-- Name: fournisseur; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fournisseur (
    idfournisseur integer NOT NULL,
    numtel character varying(45) DEFAULT NULL::character varying,
    nom character varying(45) DEFAULT NULL::character varying
);


--
-- TOC entry 226 (class 1259 OID 24880)
-- Name: fournisseur_idfournisseur_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fournisseur_idfournisseur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3519 (class 0 OID 0)
-- Dependencies: 226
-- Name: fournisseur_idfournisseur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fournisseur_idfournisseur_seq OWNED BY public.fournisseur.idfournisseur;


--
-- TOC entry 229 (class 1259 OID 24890)
-- Name: livraison; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.livraison (
    idlivraison integer NOT NULL,
    idfournisseur integer
);


--
-- TOC entry 228 (class 1259 OID 24889)
-- Name: livraison_idlivraison_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.livraison_idlivraison_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3520 (class 0 OID 0)
-- Dependencies: 228
-- Name: livraison_idlivraison_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.livraison_idlivraison_seq OWNED BY public.livraison.idlivraison;


--
-- TOC entry 215 (class 1259 OID 16591)
-- Name: login; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.login (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    prenom character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    telephone character varying(255) NOT NULL
);


--
-- TOC entry 214 (class 1259 OID 16590)
-- Name: login_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3521 (class 0 OID 0)
-- Dependencies: 214
-- Name: login_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.login_id_seq OWNED BY public.login.id;


--
-- TOC entry 231 (class 1259 OID 24902)
-- Name: matiere; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.matiere (
    idmatiere integer NOT NULL,
    nom character varying(50) DEFAULT NULL::character varying
);


--
-- TOC entry 230 (class 1259 OID 24901)
-- Name: matiere_idmatiere_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.matiere_idmatiere_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3522 (class 0 OID 0)
-- Dependencies: 230
-- Name: matiere_idmatiere_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.matiere_idmatiere_seq OWNED BY public.matiere.idmatiere;


--
-- TOC entry 237 (class 1259 OID 25218)
-- Name: matieredossage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.matieredossage (
    idmatiere integer,
    idmedinterne integer,
    dossage integer
);


--
-- TOC entry 3523 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE matieredossage; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.matieredossage IS 'TRIAL';


--
-- TOC entry 3524 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN matieredossage.idmatiere; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.matieredossage.idmatiere IS 'TRIAL';


--
-- TOC entry 3525 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN matieredossage.idmedinterne; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.matieredossage.idmedinterne IS 'TRIAL';


--
-- TOC entry 3526 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN matieredossage.dossage; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.matieredossage.dossage IS 'TRIAL';


--
-- TOC entry 232 (class 1259 OID 24919)
-- Name: medcin; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medcin (
    idmedcin integer NOT NULL,
    adresse character varying(45) DEFAULT NULL::character varying,
    specialite character varying(45) DEFAULT NULL::character varying
);


--
-- TOC entry 238 (class 1259 OID 25223)
-- Name: medexterne; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medexterne (
    idmedexterne integer NOT NULL,
    nomfirme character varying(50)
);


--
-- TOC entry 3527 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE medexterne; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.medexterne IS 'TRIAL';


--
-- TOC entry 3528 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN medexterne.idmedexterne; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medexterne.idmedexterne IS 'TRIAL';


--
-- TOC entry 3529 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN medexterne.nomfirme; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medexterne.nomfirme IS 'TRIAL';


--
-- TOC entry 239 (class 1259 OID 25228)
-- Name: medexternestock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medexternestock (
    idmedexternestock integer NOT NULL,
    numlot integer,
    dateexp date
);


--
-- TOC entry 3530 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE medexternestock; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.medexternestock IS 'TRIAL';


--
-- TOC entry 3531 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN medexternestock.idmedexternestock; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medexternestock.idmedexternestock IS 'TRIAL';


--
-- TOC entry 3532 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN medexternestock.numlot; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medexternestock.numlot IS 'TRIAL';


--
-- TOC entry 3533 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN medexternestock.dateexp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medexternestock.dateexp IS 'TRIAL';


--
-- TOC entry 233 (class 1259 OID 24955)
-- Name: medicament; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medicament (
    idmed integer NOT NULL,
    typemed public.typemed,
    modeprise public.modeprise,
    ordrequise integer,
    qtemin integer,
    remise double precision
);


--
-- TOC entry 240 (class 1259 OID 25233)
-- Name: medicamentprescrit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medicamentprescrit (
    idmedprescrit integer NOT NULL,
    idcommande integer,
    qte integer,
    dure date,
    idachat integer,
    nom character varying(45)
);


--
-- TOC entry 3534 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE medicamentprescrit; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.medicamentprescrit IS 'TRIAL';


--
-- TOC entry 3535 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.idmedprescrit; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.idmedprescrit IS 'TRIAL';


--
-- TOC entry 3536 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.idcommande; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.idcommande IS 'TRIAL';


--
-- TOC entry 3537 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.qte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.qte IS 'TRIAL';


--
-- TOC entry 3538 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.dure; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.dure IS 'TRIAL';


--
-- TOC entry 3539 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.idachat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.idachat IS 'TRIAL';


--
-- TOC entry 3540 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN medicamentprescrit.nom; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.medicamentprescrit.nom IS 'TRIAL';


--
-- TOC entry 234 (class 1259 OID 24970)
-- Name: medinterne; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medinterne (
    idmedinterne integer NOT NULL
);


--
-- TOC entry 235 (class 1259 OID 24980)
-- Name: medinternestock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medinternestock (
    idmedinternestock integer NOT NULL,
    numlot integer,
    dateexp date
);


--
-- TOC entry 236 (class 1259 OID 24990)
-- Name: medlivraison; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medlivraison (
    idlivraison integer NOT NULL,
    idmed integer NOT NULL,
    qte integer
);


--
-- TOC entry 219 (class 1259 OID 24825)
-- Name: personne; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.personne (
    idpersonne integer NOT NULL,
    nom character varying(50) DEFAULT NULL::character varying,
    prenom character varying(50) DEFAULT NULL::character varying,
    age integer
);


--
-- TOC entry 218 (class 1259 OID 24824)
-- Name: personne_idpersonne_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.personne_idpersonne_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3541 (class 0 OID 0)
-- Dependencies: 218
-- Name: personne_idpersonne_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.personne_idpersonne_seq OWNED BY public.personne.idpersonne;


--
-- TOC entry 217 (class 1259 OID 24817)
-- Name: produit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.produit (
    idprod integer NOT NULL,
    nom character varying(255) DEFAULT NULL::character varying,
    qte integer,
    prix integer
);


--
-- TOC entry 216 (class 1259 OID 24816)
-- Name: produit_idprod_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.produit_idprod_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3542 (class 0 OID 0)
-- Dependencies: 216
-- Name: produit_idprod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.produit_idprod_seq OWNED BY public.produit.idprod;


--
-- TOC entry 241 (class 1259 OID 25239)
-- Name: produitparapharmacetique; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.produitparapharmacetique (
    idpara integer NOT NULL,
    typeprod character varying(21)
);


--
-- TOC entry 3543 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE produitparapharmacetique; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.produitparapharmacetique IS 'TRIAL';


--
-- TOC entry 3544 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN produitparapharmacetique.idpara; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.produitparapharmacetique.idpara IS 'TRIAL';


--
-- TOC entry 3545 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN produitparapharmacetique.typeprod; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.produitparapharmacetique.typeprod IS 'TRIAL';


--
-- TOC entry 3272 (class 2604 OID 24848)
-- Name: achat idachat; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.achat ALTER COLUMN idachat SET DEFAULT nextval('public.achat_idachat_seq'::regclass);


--
-- TOC entry 3275 (class 2604 OID 24872)
-- Name: commande idcommande; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commande ALTER COLUMN idcommande SET DEFAULT nextval('public.commande_idcommande_seq'::regclass);


--
-- TOC entry 3276 (class 2604 OID 24884)
-- Name: fournisseur idfournisseur; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fournisseur ALTER COLUMN idfournisseur SET DEFAULT nextval('public.fournisseur_idfournisseur_seq'::regclass);


--
-- TOC entry 3279 (class 2604 OID 24893)
-- Name: livraison idlivraison; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.livraison ALTER COLUMN idlivraison SET DEFAULT nextval('public.livraison_idlivraison_seq'::regclass);


--
-- TOC entry 3265 (class 2604 OID 16594)
-- Name: login id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.login ALTER COLUMN id SET DEFAULT nextval('public.login_id_seq'::regclass);


--
-- TOC entry 3280 (class 2604 OID 24905)
-- Name: matiere idmatiere; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matiere ALTER COLUMN idmatiere SET DEFAULT nextval('public.matiere_idmatiere_seq'::regclass);


--
-- TOC entry 3268 (class 2604 OID 24828)
-- Name: personne idpersonne; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personne ALTER COLUMN idpersonne SET DEFAULT nextval('public.personne_idpersonne_seq'::regclass);


--
-- TOC entry 3266 (class 2604 OID 24820)
-- Name: produit idprod; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.produit ALTER COLUMN idprod SET DEFAULT nextval('public.produit_idprod_seq'::regclass);


--
-- TOC entry 3490 (class 0 OID 24845)
-- Dependencies: 222
-- Data for Name: achat; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.achat VALUES (3, 325, 28);
INSERT INTO public.achat VALUES (4, 100, 28);
INSERT INTO public.achat VALUES (5, 225, 28);
INSERT INTO public.achat VALUES (6, 225, 28);
INSERT INTO public.achat VALUES (7, 225, 28);
INSERT INTO public.achat VALUES (8, 1035, 29);
INSERT INTO public.achat VALUES (9, 80, 30);
INSERT INTO public.achat VALUES (10, 500, 31);
INSERT INTO public.achat VALUES (11, 600, 28);
INSERT INTO public.achat VALUES (12, 100000, 35);
INSERT INTO public.achat VALUES (13, 2250, 35);
INSERT INTO public.achat VALUES (15, 500, 33);
INSERT INTO public.achat VALUES (16, 9, 32);
INSERT INTO public.achat VALUES (19, 80, 30);
INSERT INTO public.achat VALUES (20, 91, 28);
INSERT INTO public.achat VALUES (22, 91, 28);
INSERT INTO public.achat VALUES (24, 91, 28);
INSERT INTO public.achat VALUES (26, 91, 28);
INSERT INTO public.achat VALUES (28, 91, 28);
INSERT INTO public.achat VALUES (30, 91, 28);
INSERT INTO public.achat VALUES (32, 91, 28);
INSERT INTO public.achat VALUES (34, 91, 28);
INSERT INTO public.achat VALUES (36, 91, 28);


--
-- TOC entry 3491 (class 0 OID 24856)
-- Dependencies: 223
-- Data for Name: chefpharmacie; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.chefpharmacie VALUES (1, 'lazalokmane@gmail.com', '0000');
INSERT INTO public.chefpharmacie VALUES (2, '', '');


--
-- TOC entry 3488 (class 0 OID 24833)
-- Dependencies: 220
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.client VALUES (28, '23265', 1);
INSERT INTO public.client VALUES (29, '15963', 0);
INSERT INTO public.client VALUES (30, '1653', 1);
INSERT INTO public.client VALUES (31, '76542', 0);
INSERT INTO public.client VALUES (32, '854', 1);
INSERT INTO public.client VALUES (33, '95632', 1);
INSERT INTO public.client VALUES (35, '12', 1);


--
-- TOC entry 3493 (class 0 OID 24869)
-- Dependencies: 225
-- Data for Name: commande; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.commande VALUES (3, 45000, 28);
INSERT INTO public.commande VALUES (4, 9000, 35);
INSERT INTO public.commande VALUES (5, 844, 28);
INSERT INTO public.commande VALUES (6, 0, 28);


--
-- TOC entry 3495 (class 0 OID 24881)
-- Dependencies: 227
-- Data for Name: fournisseur; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fournisseur VALUES (14, '06352', 'nadhire');
INSERT INTO public.fournisseur VALUES (15, '564455', 'hamada');
INSERT INTO public.fournisseur VALUES (16, '8452', 'gmail');
INSERT INTO public.fournisseur VALUES (17, '234', 'wilham');


--
-- TOC entry 3497 (class 0 OID 24890)
-- Dependencies: 229
-- Data for Name: livraison; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.livraison VALUES (29, 14);
INSERT INTO public.livraison VALUES (30, 15);
INSERT INTO public.livraison VALUES (31, 16);
INSERT INTO public.livraison VALUES (32, 14);
INSERT INTO public.livraison VALUES (33, 16);
INSERT INTO public.livraison VALUES (34, 16);
INSERT INTO public.livraison VALUES (35, 16);
INSERT INTO public.livraison VALUES (36, 14);
INSERT INTO public.livraison VALUES (37, 15);
INSERT INTO public.livraison VALUES (38, 15);
INSERT INTO public.livraison VALUES (39, 16);
INSERT INTO public.livraison VALUES (40, 14);
INSERT INTO public.livraison VALUES (41, 14);


--
-- TOC entry 3483 (class 0 OID 16591)
-- Dependencies: 215
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.login VALUES (15, 'clinton', '1234clinton', 'natacha', 'hillary', '12345sdvbnm', '12456asdfg');
INSERT INTO public.login VALUES (16, 'yusuySerally', '12345', 'mr yusuf', 'serally', '+1234567', 'por louis');


--
-- TOC entry 3499 (class 0 OID 24902)
-- Dependencies: 231
-- Data for Name: matiere; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.matiere VALUES (7, 'farine');
INSERT INTO public.matiere VALUES (5, 'sel');
INSERT INTO public.matiere VALUES (6, 'sucre');


--
-- TOC entry 3505 (class 0 OID 25218)
-- Dependencies: 237
-- Data for Name: matieredossage; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.matieredossage VALUES (7, 62, 100);
INSERT INTO public.matieredossage VALUES (5, 62, 120);
INSERT INTO public.matieredossage VALUES (6, 62, 1200);
INSERT INTO public.matieredossage VALUES (7, 64, 100);
INSERT INTO public.matieredossage VALUES (5, 64, 100);
INSERT INTO public.matieredossage VALUES (6, 64, 100);
INSERT INTO public.matieredossage VALUES (7, 62, 100);
INSERT INTO public.matieredossage VALUES (5, 62, 120);
INSERT INTO public.matieredossage VALUES (6, 62, 1200);
INSERT INTO public.matieredossage VALUES (7, 64, 100);
INSERT INTO public.matieredossage VALUES (5, 64, 100);
INSERT INTO public.matieredossage VALUES (6, 64, 100);


--
-- TOC entry 3500 (class 0 OID 24919)
-- Dependencies: 232
-- Data for Name: medcin; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medcin VALUES (36, 'saoula', 'f');
INSERT INTO public.medcin VALUES (41, 'birkhadem', 'f');


--
-- TOC entry 3506 (class 0 OID 25223)
-- Dependencies: 238
-- Data for Name: medexterne; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medexterne VALUES (63, 'saidale');
INSERT INTO public.medexterne VALUES (65, 'hhh');
INSERT INTO public.medexterne VALUES (66, 'remise');


--
-- TOC entry 3507 (class 0 OID 25228)
-- Dependencies: 239
-- Data for Name: medexternestock; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medexternestock VALUES (63, 10, '2021-10-14');
INSERT INTO public.medexternestock VALUES (65, 85, '2021-10-08');
INSERT INTO public.medexternestock VALUES (66, 12, '2022-10-21');


--
-- TOC entry 3501 (class 0 OID 24955)
-- Dependencies: 233
-- Data for Name: medicament; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medicament VALUES (61, 'Antibiothique', 'Orale', 1, 10, 10);
INSERT INTO public.medicament VALUES (62, 'Antibiothique', 'Nasale', 1, 10, 10);
INSERT INTO public.medicament VALUES (63, 'Antihistaminique', 'Orale', 1, 10, 20);
INSERT INTO public.medicament VALUES (64, 'Antihistaminique', 'Orale', 1, 10, 10);
INSERT INTO public.medicament VALUES (65, 'Antihistaminique', 'Orale', 1, 25, 20);
INSERT INTO public.medicament VALUES (66, 'Antihistaminique', 'Orale', 1, 10, 100);
INSERT INTO public.medicament VALUES (67, 'Antibiothique', 'Orale', 1, 30, 5);
INSERT INTO public.medicament VALUES (68, 'Antibiothique', 'Orale', 0, 4, 5);


--
-- TOC entry 3508 (class 0 OID 25233)
-- Dependencies: 240
-- Data for Name: medicamentprescrit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medicamentprescrit VALUES (61, NULL, 1, '2020-10-15', 7, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (62, NULL, 3, '2020-11-08', 8, 'lartan');
INSERT INTO public.medicamentprescrit VALUES (61, NULL, 1, '2020-11-08', 8, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (65, NULL, 10, '2020-10-21', 9, 'doliprane');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 10, '2020-10-31', 10, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (61, 3, 200, '2020-10-06', NULL, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 12, '2020-10-31', 11, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 1000, '2020-10-31', 12, 'couton');
INSERT INTO public.medicamentprescrit VALUES (61, NULL, 10, '2020-09-30', 13, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 100, '2020-10-31', 13, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 10, '2020-10-31', 15, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (64, NULL, 1, '2020-10-07', 16, 'hamra');
INSERT INTO public.medicamentprescrit VALUES (65, NULL, 10, '2020-10-07', 19, 'doliprane');
INSERT INTO public.medicamentprescrit VALUES (64, 4, 1000, '2020-10-15', NULL, 'hamra');
INSERT INTO public.medicamentprescrit VALUES (61, 7, 3, '2023-03-01', NULL, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 300, '2023-03-30', 22, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 5, '2023-03-27', 24, 'couton');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 5, '2023-03-28', 26, 'couton');
INSERT INTO public.medicamentprescrit VALUES (62, 37, 200, '2016-02-26', NULL, 'lartan');
INSERT INTO public.medicamentprescrit VALUES (62, 37, 300, '2021-03-16', NULL, 'lartan');
INSERT INTO public.medicamentprescrit VALUES (61, NULL, 1, '2020-10-15', 7, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (62, NULL, 3, '2020-11-08', 8, 'lartan');
INSERT INTO public.medicamentprescrit VALUES (61, NULL, 1, '2020-11-08', 8, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (65, NULL, 10, '2020-10-21', 9, 'doliprane');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 10, '2020-10-31', 10, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (61, 3, 200, '2020-10-06', NULL, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 12, '2020-10-31', 11, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 1000, '2020-10-31', 12, 'couton');
INSERT INTO public.medicamentprescrit VALUES (61, NULL, 10, '2020-09-30', 13, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 100, '2020-10-31', 13, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 10, '2020-10-31', 15, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (64, NULL, 1, '2020-10-07', 16, 'hamra');
INSERT INTO public.medicamentprescrit VALUES (65, NULL, 10, '2020-10-07', 19, 'doliprane');
INSERT INTO public.medicamentprescrit VALUES (64, 4, 1000, '2020-10-15', NULL, 'hamra');
INSERT INTO public.medicamentprescrit VALUES (61, 7, 3, '2023-03-01', NULL, 'sarokh');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 3, '2023-03-26', 20, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (67, NULL, 300, '2023-03-30', 22, 'Paracetamol');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (59, NULL, 5, '2023-03-27', 22, 'bavatte');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 5, '2023-03-27', 24, 'couton');
INSERT INTO public.medicamentprescrit VALUES (60, NULL, 5, '2023-03-28', 26, 'couton');
INSERT INTO public.medicamentprescrit VALUES (62, 37, 200, '2016-02-26', NULL, 'lartan');
INSERT INTO public.medicamentprescrit VALUES (62, 37, 300, '2021-03-16', NULL, 'lartan');


--
-- TOC entry 3502 (class 0 OID 24970)
-- Dependencies: 234
-- Data for Name: medinterne; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medinterne VALUES (61);
INSERT INTO public.medinterne VALUES (62);
INSERT INTO public.medinterne VALUES (64);
INSERT INTO public.medinterne VALUES (67);
INSERT INTO public.medinterne VALUES (68);


--
-- TOC entry 3503 (class 0 OID 24980)
-- Dependencies: 235
-- Data for Name: medinternestock; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medinternestock VALUES (61, 10, '2021-10-30');
INSERT INTO public.medinternestock VALUES (62, 20, '2021-10-29');
INSERT INTO public.medinternestock VALUES (64, 156, '2022-10-13');
INSERT INTO public.medinternestock VALUES (67, 0, '2023-04-09');
INSERT INTO public.medinternestock VALUES (68, 23, '2024-02-29');


--
-- TOC entry 3504 (class 0 OID 24990)
-- Dependencies: 236
-- Data for Name: medlivraison; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medlivraison VALUES (29, 59, 12);
INSERT INTO public.medlivraison VALUES (29, 60, 1200);
INSERT INTO public.medlivraison VALUES (29, 63, 3);
INSERT INTO public.medlivraison VALUES (30, 63, 10);
INSERT INTO public.medlivraison VALUES (32, 60, 600);
INSERT INTO public.medlivraison VALUES (33, 63, 100);
INSERT INTO public.medlivraison VALUES (34, 65, 100);
INSERT INTO public.medlivraison VALUES (35, 65, 1);
INSERT INTO public.medlivraison VALUES (36, 65, 100);
INSERT INTO public.medlivraison VALUES (37, 65, 100);
INSERT INTO public.medlivraison VALUES (38, 65, 100);
INSERT INTO public.medlivraison VALUES (40, 65, 99);
INSERT INTO public.medlivraison VALUES (41, 63, 100);
INSERT INTO public.medlivraison VALUES (41, 65, 100);


--
-- TOC entry 3487 (class 0 OID 24825)
-- Dependencies: 219
-- Data for Name: personne; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.personne VALUES (1, 'zitouni', 'lokmane', 10);
INSERT INTO public.personne VALUES (2, 'aimen', 'hammani', 10);
INSERT INTO public.personne VALUES (3, 'alouane', 'wail', 15);
INSERT INTO public.personne VALUES (4, 'lokmane', 'zitou', 12);
INSERT INTO public.personne VALUES (5, 'abdelbari', 'zitouni', 21);
INSERT INTO public.personne VALUES (6, 'hasen', 'selij', 21);
INSERT INTO public.personne VALUES (21, 'salim', 'salij', 65);
INSERT INTO public.personne VALUES (28, 'Hammani', 'aimene', 7);
INSERT INTO public.personne VALUES (29, 'hammani', 'wail', 54);
INSERT INTO public.personne VALUES (30, 'alouane', 'wail', 45);
INSERT INTO public.personne VALUES (31, 'zitouni', 'abdelbari', 45);
INSERT INTO public.personne VALUES (32, 'zitouni', 'moumou', 4);
INSERT INTO public.personne VALUES (33, 'salij', 'salime', 54);
INSERT INTO public.personne VALUES (35, 'jdid', 'jdid', 12);
INSERT INTO public.personne VALUES (36, 'hasen', 'selij', 26);
INSERT INTO public.personne VALUES (41, 'meftah-key', 'salhi', 23);
INSERT INTO public.personne VALUES (42, 'lok', 'lok', 12);
INSERT INTO public.personne VALUES (43, 'salim', 'wilham', 23);


--
-- TOC entry 3485 (class 0 OID 24817)
-- Dependencies: 217
-- Data for Name: produit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.produit VALUES (59, 'bavatte', 390, 50);
INSERT INTO public.produit VALUES (60, 'couton', 980, 100);
INSERT INTO public.produit VALUES (61, 'sarokh', 100, 250);
INSERT INTO public.produit VALUES (62, 'lartan', 200, 300);
INSERT INTO public.produit VALUES (63, 'sarokh', 713, 100);
INSERT INTO public.produit VALUES (64, 'hamra', 200, 10);
INSERT INTO public.produit VALUES (65, 'doliprane', 490, 10);
INSERT INTO public.produit VALUES (66, 'remise', 100, 100);
INSERT INTO public.produit VALUES (1, 'test', 34, 90);
INSERT INTO public.produit VALUES (2, 'test', 34, 90);
INSERT INTO public.produit VALUES (67, 'test', 34, 12);


--
-- TOC entry 3509 (class 0 OID 25239)
-- Dependencies: 241
-- Data for Name: produitparapharmacetique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.produitparapharmacetique VALUES (59, 'Cosmétique');
INSERT INTO public.produitparapharmacetique VALUES (60, 'Hygienique');


--
-- TOC entry 3546 (class 0 OID 0)
-- Dependencies: 221
-- Name: achat_idachat_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.achat_idachat_seq', 37, true);


--
-- TOC entry 3547 (class 0 OID 0)
-- Dependencies: 224
-- Name: commande_idcommande_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.commande_idcommande_seq', 6, true);


--
-- TOC entry 3548 (class 0 OID 0)
-- Dependencies: 226
-- Name: fournisseur_idfournisseur_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fournisseur_idfournisseur_seq', 17, true);


--
-- TOC entry 3549 (class 0 OID 0)
-- Dependencies: 228
-- Name: livraison_idlivraison_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.livraison_idlivraison_seq', 41, true);


--
-- TOC entry 3550 (class 0 OID 0)
-- Dependencies: 214
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.login_id_seq', 16, true);


--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 230
-- Name: matiere_idmatiere_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.matiere_idmatiere_seq', 7, true);


--
-- TOC entry 3552 (class 0 OID 0)
-- Dependencies: 218
-- Name: personne_idpersonne_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.personne_idpersonne_seq', 43, true);


--
-- TOC entry 3553 (class 0 OID 0)
-- Dependencies: 216
-- Name: produit_idprod_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.produit_idprod_seq', 67, true);


--
-- TOC entry 3291 (class 2606 OID 24850)
-- Name: achat achat_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.achat
    ADD CONSTRAINT achat_pkey PRIMARY KEY (idachat);


--
-- TOC entry 3293 (class 2606 OID 24862)
-- Name: chefpharmacie chefpharmacie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chefpharmacie
    ADD CONSTRAINT chefpharmacie_pkey PRIMARY KEY (idchef);


--
-- TOC entry 3289 (class 2606 OID 24838)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (idclient);


--
-- TOC entry 3295 (class 2606 OID 24874)
-- Name: commande commande_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commande
    ADD CONSTRAINT commande_pkey PRIMARY KEY (idcommande);


--
-- TOC entry 3297 (class 2606 OID 24888)
-- Name: fournisseur fournisseur_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fournisseur
    ADD CONSTRAINT fournisseur_pkey PRIMARY KEY (idfournisseur);


--
-- TOC entry 3299 (class 2606 OID 24895)
-- Name: livraison livraison_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.livraison
    ADD CONSTRAINT livraison_pkey PRIMARY KEY (idlivraison);


--
-- TOC entry 3301 (class 2606 OID 24908)
-- Name: matiere matiere_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matiere
    ADD CONSTRAINT matiere_pkey PRIMARY KEY (idmatiere);


--
-- TOC entry 3303 (class 2606 OID 24925)
-- Name: medcin medcin_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medcin
    ADD CONSTRAINT medcin_pkey PRIMARY KEY (idmedcin);


--
-- TOC entry 3305 (class 2606 OID 24959)
-- Name: medicament medicament_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medicament
    ADD CONSTRAINT medicament_pkey PRIMARY KEY (idmed);


--
-- TOC entry 3307 (class 2606 OID 24974)
-- Name: medinterne medinterne_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medinterne
    ADD CONSTRAINT medinterne_pkey PRIMARY KEY (idmedinterne);


--
-- TOC entry 3309 (class 2606 OID 24984)
-- Name: medinternestock medinternestock_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medinternestock
    ADD CONSTRAINT medinternestock_pkey PRIMARY KEY (idmedinternestock);


--
-- TOC entry 3311 (class 2606 OID 24994)
-- Name: medlivraison medlivraison_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medlivraison
    ADD CONSTRAINT medlivraison_pkey PRIMARY KEY (idlivraison, idmed);


--
-- TOC entry 3287 (class 2606 OID 24832)
-- Name: personne personne_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personne
    ADD CONSTRAINT personne_pkey PRIMARY KEY (idpersonne);


--
-- TOC entry 3315 (class 2606 OID 25227)
-- Name: medexterne pk_medexterne; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medexterne
    ADD CONSTRAINT pk_medexterne PRIMARY KEY (idmedexterne);


--
-- TOC entry 3317 (class 2606 OID 25232)
-- Name: medexternestock pk_medexternestock; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medexternestock
    ADD CONSTRAINT pk_medexternestock PRIMARY KEY (idmedexternestock);


--
-- TOC entry 3322 (class 2606 OID 25243)
-- Name: produitparapharmacetique pk_produitparapharmacetique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.produitparapharmacetique
    ADD CONSTRAINT pk_produitparapharmacetique PRIMARY KEY (idpara);


--
-- TOC entry 3285 (class 2606 OID 24823)
-- Name: produit produit_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.produit
    ADD CONSTRAINT produit_pkey PRIMARY KEY (idprod);


--
-- TOC entry 3318 (class 1259 OID 25237)
-- Name: com; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX com ON public.medicamentprescrit USING btree (idcommande);


--
-- TOC entry 3312 (class 1259 OID 25221)
-- Name: mat; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mat ON public.matieredossage USING btree (idmatiere);


--
-- TOC entry 3319 (class 1259 OID 25236)
-- Name: medachat_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX medachat_idx ON public.medicamentprescrit USING btree (idachat);


--
-- TOC entry 3313 (class 1259 OID 25222)
-- Name: medint; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX medint ON public.matieredossage USING btree (idmedinterne);


--
-- TOC entry 3320 (class 1259 OID 25238)
-- Name: medp_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX medp_idx ON public.medicamentprescrit USING btree (idmedprescrit);


--
-- TOC entry 3339 (class 2620 OID 25285)
-- Name: client historique_client_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER historique_client_trigger AFTER INSERT OR DELETE OR UPDATE ON public.client FOR EACH ROW EXECUTE FUNCTION public.historique_client();


--
-- TOC entry 3324 (class 2606 OID 24851)
-- Name: achat achat_idclient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.achat
    ADD CONSTRAINT achat_idclient_fkey FOREIGN KEY (idclient) REFERENCES public.client(idclient);


--
-- TOC entry 3325 (class 2606 OID 24863)
-- Name: chefpharmacie chefpharmacie_idchef_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chefpharmacie
    ADD CONSTRAINT chefpharmacie_idchef_fkey FOREIGN KEY (idchef) REFERENCES public.personne(idpersonne);


--
-- TOC entry 3323 (class 2606 OID 24839)
-- Name: client client_idclient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_idclient_fkey FOREIGN KEY (idclient) REFERENCES public.personne(idpersonne);


--
-- TOC entry 3326 (class 2606 OID 24875)
-- Name: commande commande_idclient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commande
    ADD CONSTRAINT commande_idclient_fkey FOREIGN KEY (idclient) REFERENCES public.client(idclient);


--
-- TOC entry 3336 (class 2606 OID 25254)
-- Name: medexterne externemed; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medexterne
    ADD CONSTRAINT externemed FOREIGN KEY (idmedexterne) REFERENCES public.medicament(idmed) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3337 (class 2606 OID 25259)
-- Name: medexternestock externemedstock; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medexternestock
    ADD CONSTRAINT externemedstock FOREIGN KEY (idmedexternestock) REFERENCES public.medexterne(idmedexterne) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3327 (class 2606 OID 24896)
-- Name: livraison livraison_idfournisseur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.livraison
    ADD CONSTRAINT livraison_idfournisseur_fkey FOREIGN KEY (idfournisseur) REFERENCES public.fournisseur(idfournisseur);


--
-- TOC entry 3334 (class 2606 OID 25244)
-- Name: matieredossage mat; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matieredossage
    ADD CONSTRAINT mat FOREIGN KEY (idmatiere) REFERENCES public.matiere(idmatiere) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3328 (class 2606 OID 24926)
-- Name: medcin medcin_idmedcin_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medcin
    ADD CONSTRAINT medcin_idmedcin_fkey FOREIGN KEY (idmedcin) REFERENCES public.personne(idpersonne);


--
-- TOC entry 3329 (class 2606 OID 24960)
-- Name: medicament medicament_idmed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medicament
    ADD CONSTRAINT medicament_idmed_fkey FOREIGN KEY (idmed) REFERENCES public.produit(idprod);


--
-- TOC entry 3335 (class 2606 OID 25249)
-- Name: matieredossage medint; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matieredossage
    ADD CONSTRAINT medint FOREIGN KEY (idmedinterne) REFERENCES public.medinterne(idmedinterne) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3330 (class 2606 OID 24975)
-- Name: medinterne medinterne_idmedinterne_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medinterne
    ADD CONSTRAINT medinterne_idmedinterne_fkey FOREIGN KEY (idmedinterne) REFERENCES public.medicament(idmed);


--
-- TOC entry 3331 (class 2606 OID 24985)
-- Name: medinternestock medinternestock_idmedinternestock_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medinternestock
    ADD CONSTRAINT medinternestock_idmedinternestock_fkey FOREIGN KEY (idmedinternestock) REFERENCES public.medinterne(idmedinterne);


--
-- TOC entry 3332 (class 2606 OID 24995)
-- Name: medlivraison medlivraison_idlivraison_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medlivraison
    ADD CONSTRAINT medlivraison_idlivraison_fkey FOREIGN KEY (idlivraison) REFERENCES public.livraison(idlivraison);


--
-- TOC entry 3333 (class 2606 OID 25000)
-- Name: medlivraison medlivraison_idmed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medlivraison
    ADD CONSTRAINT medlivraison_idmed_fkey FOREIGN KEY (idmed) REFERENCES public.produit(idprod);


--
-- TOC entry 3338 (class 2606 OID 25279)
-- Name: produitparapharmacetique prodpara; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.produitparapharmacetique
    ADD CONSTRAINT prodpara FOREIGN KEY (idpara) REFERENCES public.produit(idprod) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2023-06-01 20:31:12

--
-- PostgreSQL database dump complete
--

